const express = require("express");
const { getAll, getLastOrder, insert, runQuery, getLastId} = require("../db/conexion");
const router = express.Router();
const multer = require("multer");
const path = require("path");
const exphbs = require('express-handlebars');
const hbs = require('hbs');

hbs.registerHelper('eq', function (a, b) {
    return a === b;
});


// Configurar multer para la carga de archivos
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'public/images/uploads/');
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + path.extname(file.originalname));
    }
});
const upload = multer({ storage: storage });



router.get('/', (req, res) => {
    res.render("admin/index");
});

//controladores
const AdminIntegrantesController = require('../controllers/admin/integrantecontrollers')
const MediaController = require('../controllers/admin/mediacontrollers')
const TipoMediaController = require('../controllers/admin/tipomediacontrollers')
const inicioController = require('../controllers/admin/inicioControllers');

router.get('/', inicioController.index);

//Integrante

router.get('/integrante/listar',AdminIntegrantesController.index );

router.get('/integrante/crear', AdminIntegrantesController.create);

router.post('/integrante/create',AdminIntegrantesController.store);

router.get('/integrante/:matricula', AdminIntegrantesController.show);

router.post('/integrante/delete/:matricula',AdminIntegrantesController.destroy);

router.get('/integrante/edit/:matricula', AdminIntegrantesController.edit);

router.post('/integrante/update/:matricula', AdminIntegrantesController.update);


// Media
router.get('/media/listar',MediaController.index);

router.get('/media/crear',MediaController.create);

router.post('/media/create', upload.single('imagen'), MediaController.store);

router.get('/media/:id', MediaController.show);

router.post('/media/delete/:id',MediaController.destroy);

router.get('/media/edit/:id', MediaController.edit);

router.post('/media/update/:id', upload.single('imagen'), MediaController.update);


// Tipo de Medias
router.get('/tipomedia/listar',TipoMediaController.index);

router.get('/tipomedia/crear',TipoMediaController.create);

router.post('/tipomedia/create', TipoMediaController.store);

router.get('/tipomedia/:id_tmedia', TipoMediaController.show);

router.post('/tipomedia/delete/:id_tmedia',TipoMediaController.destroy);

router.get('/tipomedia/edit/:id_tmedia', TipoMediaController.edit);

router.post('/tipomedia/update/:id_tmedia', TipoMediaController.update);




module.exports = router;



