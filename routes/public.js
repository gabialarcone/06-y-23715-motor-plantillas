// routes/public.js
const express = require("express");
const router = express.Router();
const PublicControllers = require("../controllers/public/publicControllers");
require("dotenv").config();

router.use((req, res, next) => {
    res.locals.repositorio = process.env.REPOSITORIO;
    res.locals.nombre = process.env.NOMBRE;
    res.locals.materia = process.env.MATERIA;
    res.locals.matricula = process.env.MATRICULA;
    next();
});

// definir todas rutas del proyecto
router.get("/", PublicControllers.renderIndex);

router.get("/curso/", PublicControllers.renderCurso);

router.get("/word_cloud/", PublicControllers.renderWordCloud);

router.get("/integrante/:matricula", PublicControllers.getIntegranteByMatricula);

module.exports = router;