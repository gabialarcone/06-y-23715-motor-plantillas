const { getAll, getLastOrder, runQuery } = require('../db/conexion');

const MediaModel = {
    getAllMedia: async function (filters) {
        let query = 'SELECT m.*, i.nombre AS integranteNombre FROM media m LEFT JOIN integrante i ON m.matricula = i.matricula WHERE m.activo = 1';
        const params = [];

        if (filters) {
            for (const prop in filters) {
                if (filters[prop]) {
                    if (prop === 'titulo') {
                        query += ` AND m.${prop} LIKE ?`;
                        params.push(`%${filters[prop]}%`);
                    } else if (prop === 'nombre') {
                        query += ` AND i.${prop} LIKE ?`;
                        params.push(`%${filters[prop]}%`);
                    } else if (prop === 'id_media') {
                        query += ` AND m.${prop} = ?`;
                        params.push(filters[prop]);
                    } else {
                        query += ` AND m.${prop} = ?`;
                        params.push(filters[prop]);
                    }
                }
            }
        }

        return await getAll(query, params);
    },

    getIntegrantes: async function () {
        return await getAll('SELECT * FROM integrante');
    },

    getTiposMedia: async function () {
        return await getAll('SELECT * FROM tipomedia');
    },

    createMedia: async function (data) {
        const { src, url, titulo, id_tmedia, matricula, activo } = data;
        const ultimoOrden = await getLastOrder('media');
        const nuevoOrden = ultimoOrden + 1;

        const query = `
            INSERT INTO media (src, url, titulo, id_tmedia, matricula, activo, nro_orden)
            VALUES (?, ?, ?, ?, ?, ?, ?)
        `;
        const params = [src, url, titulo, id_tmedia, matricula, activo, nuevoOrden];
        return await runQuery(query, params);
    },

    getMediaById: async function (id) {
        return await getAll('SELECT * FROM media WHERE id = ?', [id]);
    },

    updateMedia: async function (id, data) {
        const updates = [];
        const params = [];

        for (const key in data) {
            updates.push(`${key} = ?`);
            params.push(data[key]);
        }

        params.push(id);

        const query = `UPDATE media SET ${updates.join(', ')} WHERE id = ?`;
        return await runQuery(query, params);
    },

    deactivateMedia: async function (id) {
        const query = 'UPDATE media SET activo = 0 WHERE id = ?';
        return await runQuery(query, [id]);
    }
};

module.exports = MediaModel;
