const { getAll, insert, getLastOrder, getLastId, runQuery, run } = require('../db/conexion');

const TipoMediaModel = {
    getAllTipoMedia: async function (filters) {
        let query = "SELECT * FROM tipomedia WHERE activo = 1";
        const params = [];

        if (filters) {
            for (const prop in filters) {
                if (filters[prop]) {
                    if (prop === 'nombre' || prop === 'descripcion') {
                        query += ` AND ${prop} LIKE ?`;
                        params.push(`%${filters[prop]}%`);
                    } else {
                        query += ` AND ${prop} = ?`;
                        params.push(filters[prop]);
                    }
                }
            }
        }

        return await getAll(query, params);
    },

    createTipoMedia: async function (data) {
        const { nombre, activo } = data;
        const ultimoOrden = await getLastOrder("tipomedia");
        const nuevoOrden = ultimoOrden + 1;
        const ultimoId = await getLastId("tipomedia");
        const nuevoId = ultimoId + 1;
        const isActive = activo ? 1 : 0;

        const query = `
            INSERT INTO tipomedia (nro_orden, id_tmedia, nombre, activo)
            VALUES (?, ?, ?, ?)
        `;
        const params = [nuevoOrden, nuevoId, nombre, isActive];
        return await insert(query, params);
    },

    getTipoMediaById: async function (id_tmedia) {
        const results = await getAll('SELECT * FROM tipomedia WHERE id_tmedia = ?', [id_tmedia]);
        return results.length > 0 ? results[0] : null; // Asegúrate de retornar un solo objeto
    },


    updateTipoMedia: async function (id_tmedia, data) {
        const updates = [];
        const params = [];

        for (const key in data) {
            updates.push(`${key} = ?`);
            params.push(data[key]);
        }

        params.push(id_tmedia);

        const query = `UPDATE tipomedia SET ${updates.join(', ')} WHERE id_tmedia = ?`;
        return await run(query, params);
    },

    deactivateTipoMedia: async function (id_tmedia) {
        const query = 'UPDATE tipomedia SET activo = 0 WHERE id_tmedia = ?';
        return await runQuery(query, [id_tmedia]);
    }
};

module.exports = TipoMediaModel;
