const { runQuery, getAll } = require('../db/conexion');

class IntegranteModel {
    static async getAllIntegrantes(query, params) {
        return getAll(query, params);
    }

    static async getLastOrder() {
        const query = 'SELECT MAX(nro_orden) as max_order FROM integrante';
        const rows = await runQuery(query, []);
        if (rows && rows.length > 0 && rows[0].max_order) {
            return rows[0].max_order;
        } else {
            return 0; // O un valor predeterminado si no se encontraron resultados
        }
    }


    static async insertIntegrante(integrante) {
        const query = 'INSERT INTO integrante (nro_orden, matricula, nombre, apellido, pagina, activo) VALUES (?, ?, ?, ?, ?, ?)';
        return runQuery(query, integrante);
    }

    static async getIntegranteByMatricula(matricula) {
        const query = 'SELECT * FROM integrante WHERE matricula = ?';
        const results = await getAll(query, [matricula]);
        return results[0] || null; // Retorna un solo objeto
    }

    static async updateIntegrante(integrante) {
        const query = 'UPDATE integrante SET nombre = ?, apellido = ? WHERE matricula = ?';
        return runQuery(query, integrante);
    }

    static async deactivateIntegrante(matricula) {
        const query = 'UPDATE integrante SET activo = 0 WHERE matricula = ?';
        return runQuery(query, [matricula]);
    }
}

module.exports = IntegranteModel;
