const {getAll} = require("../db/conexion");


const AuthModel = {
    getUserByEmail: async (email) => {
        try {
            const query = 'SELECT * FROM usuarios WHERE email = ?';
            const result = await getAll(query, [email]);

            if (!result || result.length === 0) {
                return null; // No se encontró el usuario
            }

            return result[0];
        } catch (error) {
            console.error("Error al obtener el usuario por correo electrónico:", error);
            throw error;
        }
    }
};

module.exports = AuthModel;

