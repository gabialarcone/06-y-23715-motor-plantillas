// validations/tipoMediaValidation.js

const Joi = require('joi');

const tipoMediaEditSchema = Joi.object({
    nombre: Joi.string().min(2).max(50).required().messages({
        'string.base': 'El nombre debe ser un texto',
        'string.empty': 'El nombre no puede estar vacío',
        'string.min': 'El nombre debe tener al menos 2 caracteres',
        'string.max': 'El nombre debe tener una longitud máxima de 50 caracteres',
        'any.required': 'El nombre es obligatorio'
    }),
    activo: Joi.boolean().truthy('1').falsy('0').required().messages({
        'boolean.base': 'Activo debe ser un valor booleano',
        'any.required': 'Activo es obligatorio'
    })
});

module.exports = {
    tipoMediaEditSchema
};
