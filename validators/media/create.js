const Joi = require('joi');

const mediaSchema = Joi.object({
    url: Joi.string().uri().allow('').optional().messages({
        'string.uri': 'La URL debe ser una URL válida'
    }),
    titulo: Joi.string().min(3).required().messages({
        'string.base': 'El título debe ser un texto',
        'string.empty': 'El título no puede estar vacío',
        'string.min': 'El título debe tener al menos 3 caracteres',
        'any.required': 'El título es obligatorio'
    }),
    id_tmedia: Joi.number().integer().required().messages({
        'number.base': 'El tipo de media debe ser un número',
        'any.required': 'El tipo de media es obligatorio'
    }),
    matricula: Joi.string().min(3).regex(/^[a-zA-Z0-9]+$/).required().messages({
        'string.base': 'La matrícula debe ser un texto',
        'string.empty': 'La matrícula no puede estar vacía',
        'string.min': 'La matrícula debe tener al menos 3 caracteres alfanuméricos',
        'string.pattern.base': 'La matrícula solo puede contener caracteres alfanuméricos',
        'any.required': 'La matrícula es obligatoria'
    }),
    activo: Joi.boolean().required().messages({
        'boolean.base': 'Activo debe ser un valor booleano',
        'any.required': 'Activo es obligatorio'
    })
});

module.exports = {
    mediaSchema
};
