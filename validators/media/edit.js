const Joi = require('joi');

const mediaEditSchema = Joi.object({
    url: Joi.string().uri().allow('').messages({
        'string.uri': 'La URL debe ser una URI válida'
    }),
    titulo: Joi.string().max(50).required().messages({
        'string.base': 'El título debe ser un texto',
        'string.empty': 'El título no puede estar vacío',
        'string.max': 'El título debe tener una longitud máxima de 50 caracteres',
        'any.required': 'El título es obligatorio'
    }),
    id_tmedia: Joi.number().integer().required().messages({
        'number.base': 'El tipo de media debe ser un número entero',
        'any.required': 'Debe seleccionar un tipo de media'
    }),
    matricula: Joi.string().required().messages({
        'string.base': 'La matrícula debe ser un texto',
        'any.required': 'Debe seleccionar un integrante'
    }),
    activo: Joi.boolean().required().messages({
        'boolean.base': 'Activo debe ser un valor booleano',
        'any.required': 'Activo es obligatorio'
    })
});

module.exports = {
    mediaEditSchema
};
