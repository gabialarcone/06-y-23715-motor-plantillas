const Joi = require("joi");

const integranteSchema = Joi.object({
    nombre: Joi.string().min(2).regex(/^[a-zA-Z]+$/).required().messages({
        'string.base': 'El nombre debe ser un texto',
        'string.empty': 'El nombre no puede estar vacío',
        'string.min': 'El nombre debe tener al menos 2 letras',
        'string.pattern.base': 'El nombre solo puede contener letras',
        'any.required': 'El nombre es obligatorio'
    }),
    apellido: Joi.string().min(2).regex(/^[a-zA-Z]+$/).required().messages({
        'string.base': 'El apellido debe ser un texto',
        'string.empty': 'El apellido no puede estar vacío',
        'string.min': 'El apellido debe tener al menos 2 letras',
        'string.pattern.base': 'El apellido solo puede contener letras',
        'any.required': 'El apellido es obligatorio'
    }),
    matricula: Joi.string().min(3).regex(/^[a-zA-Z0-9]+$/).required().messages({
        'string.base': 'La matrícula debe ser un texto',
        'string.empty': 'La matrícula no puede estar vacía',
        'string.min': 'La matrícula debe tener al menos 3 caracteres alfanuméricos',
        'string.pattern.base': 'La matrícula solo puede contener caracteres alfanuméricos',
        'any.required': 'La matrícula es obligatoria'
    }),
    pagina: Joi.string().optional(),
    activo: Joi.boolean().required().messages({
        'boolean.base': 'Activo debe ser un valor booleano',
        'any.required': 'Activo es obligatorio'
    })
});

module.exports = {
    integranteSchema
};