// validations/integranteValidation.js

const Joi = require('joi');

const integranteEditSchema = Joi.object({
    nombre: Joi.string().min(2).regex(/^[a-zA-Z]+$/).required().messages({
        'string.base': 'El nombre debe ser un texto',
        'string.empty': 'El nombre no puede estar vacío',
        'string.min': 'El nombre debe tener al menos 2 letras',
        'string.pattern.base': 'El nombre solo puede contener letras',
        'any.required': 'El nombre es obligatorio'
    }),
    apellido: Joi.string().min(2).regex(/^[a-zA-Z]+$/).required().messages({
        'string.base': 'El apellido debe ser un texto',
        'string.empty': 'El apellido no puede estar vacío',
        'string.min': 'El apellido debe tener al menos 2 letras',
        'string.pattern.base': 'El apellido solo puede contener letras',
        'any.required': 'El apellido es obligatorio'
    }),
    activo: Joi.boolean().required().messages({
        'boolean.base': 'Activo debe ser un valor booleano',
        'any.required': 'Activo es obligatorio'
    })
});

module.exports = {
    integranteEditSchema
};

