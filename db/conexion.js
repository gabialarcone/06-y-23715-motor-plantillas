const sqlite3 = require('sqlite3').verbose();
const path = require('path');
const dbPath = path.resolve(__dirname, 'integrantes.sqlite');
const db = new sqlite3.Database(dbPath);
const { open } = require('sqlite');


db.serialize(() => {
    db.run('SELECT 1', [], (err) => {
        if (err) {
            console.error('Error al conectar con la base de datos:', err);
        } else {
            console.log('Conexión exitosa con la base de datos.');
            //db.run("select * from integrante");
        }
    });
});

async function getAll(query, parametros){
    return new Promise((resolve, reject)=>{
        db.all(query, parametros, (error, rows)=>{
            if (error) {
                reject(error);
            } else{
                resolve(rows);
            }
        });
    });
}



async function getLastOrder(){
    const result = await getAll("select  Max(nro_orden) as maxOrden from integrante");
    return result[0].maxOrden;
}

async function insert(query, parametros) {
    return new Promise((resolve, reject) => {
        db.run(query, parametros, function(error) {
            if (error) {
                reject(error);
            } else {
                resolve(this.lastID);
            }
        });
    });
}

async function getLastId(tipomedia) {
    const result = await getAll(`SELECT MAX(id_tmedia) as maxId FROM ${tipomedia}`);
    return result[0].maxId;
}


function runQuery(query, params) {
    return new Promise((resolve, reject) => {
        db.run(query, params, function (err) {
            if (err) {
                return reject(err);
            }
            resolve(this.lastID); // Devuelve el ID de la última fila insertada
        });
    });
}

async function get(query, params) {
    const db = await openDb();
    const result = await db.get(query, params);
    await db.close();
    return result;
}

async function openDb() {
    return open({
        filename: 'db/integrantes.sqlite',  // Aquí cambias el nombre del archivo de la base de datos
        driver: sqlite3.Database
    });
}



async function run(sql, params) {
    const db = await openDb();
    try {
        await db.run(sql, params);
    } finally {
        await db.close();
    }
}







module.exports = {
    db,
    getAll,
    insert,
    getLastOrder,
    getLastId,
    runQuery,
    run,
    get,

}

