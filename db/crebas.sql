CREATE TABLE IF NOT EXISTS "integrante" (
	"matricula" TEXT NOT NULL UNIQUE,
	"nombre" TEXT,
	"apellido" TEXT,
	"pagina" TEXT,
	"activo" INTEGER,
	"nro_orden" INTEGER,
	PRIMARY KEY("matricula"),
	FOREIGN KEY ("matricula") REFERENCES "media"("matricula")
	ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE IF NOT EXISTS "media" (
	"id" INTEGER NOT NULL UNIQUE,
	"nombre" TEXT,
	"matricula" TEXT,
	"url" TEXT,
	"src" TEXT,
	"id_tmedia" INTEGER,
	"titulo" TEXT,
	"activo" INTEGER,
	"nro_orden" INTEGER,
	PRIMARY KEY("id"),
	FOREIGN KEY ("id_tmedia") REFERENCES "tipomedia"("id_tmedia")
	ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE IF NOT EXISTS "tipomedia" (
	"id_tmedia" INTEGER NOT NULL UNIQUE,
	"nombre" TEXT,
	"activo" INTEGER,
	"nro_orden" INTEGER,
	PRIMARY KEY("id_tmedia")	
);

CREATE TABLE IF NOT EXISTS "usuarios" (
	"id" INTEGER NOT NULL UNIQUE,
	"email" TEXT,
	"password" TEXT,
	"super_usuario" INTEGER,
	"matricula" TEXT,
	PRIMARY KEY("id"),
	FOREIGN KEY ("matricula") REFERENCES "integrante"("matricula")
	ON UPDATE NO ACTION ON DELETE NO ACTION
);
