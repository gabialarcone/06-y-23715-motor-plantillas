insert into integrante(matricula, nombre, apellido, pagina, activo, nro_orden) VALUES
    ('Y23715', 'Gabriela', 'Espinola Alarcón', '/Y23715', 1, 1);

insert into integrante(matricula, nombre, apellido, pagina, activo, nro_orden)VALUES
    ('Y26230', 'Zuanny Librada', 'Ortiz Quintana', '/Y26230', 1, 2);

insert into integrante(matricula, nombre, apellido, pagina, activo, nro_orden)VALUES
    ('Y18433', 'Nicolas', 'Gimenez', '/Y184330', 1, 3 );

insert into integrante(matricula, nombre, apellido, pagina, activo, nro_orden)VALUES
    ('UG0095', 'Diego', 'Ramirez', '/UG0095', 1, 4);

insert into integrante(matricula, nombre, apellido, pagina, activo, nro_orden)VALUES
    ('Y12954', 'Yenifer', 'Aguilera', '/12954', 1, 5);




insert into tipomedia(id_tmedia, nombre, activo, nro_orden)VALUES
    (1, 'Youtube', 1, 1);

insert into tipomedia(id_tmedia, nombre, activo, nro_orden)VALUES
    (2, 'Imagen' , 1, 2);

insert into tipomedia(id_tmedia, nombre, activo, nro_orden)VALUES
    (3, 'Dibujo' , 1, 3);



insert into media(id, nombre, matricula, url, src, id_tmedia, titulo, activo, nro_orden)values
    (1, 'Youtube', 'Y23715', 'https://www.youtube.com/embed/YehKucEI-Gc', NULL, 1, 'Video Favorito de Youtube', 1, 1);

insert into media(id, nombre, matricula, url, src, id_tmedia, titulo, activo, nro_orden)values
    (2, 'Youtube', 'UG0095', 'https://www.youtube.com/embed/210R0ozmLwg?si=bbjXLbzQS8fBaoTz', NULL, 1, 'Video Favorito de Youtube', 1, 2);

insert into media(id, nombre, matricula, url, src, id_tmedia, titulo, activo, nro_orden)values
    (3, 'Youtube', 'Y12954', 'https://www.youtube.com/embed/DjeGUE9ya5s&t=737s', NULL, 1, 'Video Favorito de Youtube', 1, 3);

insert into media(id, nombre, matricula, url, src, id_tmedia, titulo, activo, nro_orden)values
    (4, 'Youtube', 'Y18433', 'https://www.youtube.com/embed/YZ8j6iO0ulw?si=3qmb7GQ0CxHN2C3Q', NULL, 1, 'Video Favorito de Youtube', 1, 4);

insert into media(id, nombre, matricula, url, src, id_tmedia, titulo, activo, nro_orden)values
    (5, 'Youtube', 'Y26230', 'https://www.youtube.com/embed/Qes1RMK9a50?si=U5YTt8otRVQc1AhkQ', NULL, 1, 'Video Favorito de Youtube', 1, 5);


insert into media(id, nombre, matricula, url, src, id_tmedia, titulo, activo, nro_orden)values
    (6, 'Imagen', 'Y23715', NULL, '/Images/Bob Esponja.jpeg', 2, 'Imagen que me representa', 1, 6);

insert into media(id, nombre, matricula, url, src, id_tmedia, titulo, activo, nro_orden)values
    (7, 'Imagen', 'UG0095', NULL, '/Images/imagen1Diego.jpeg', 2, 'Imagen que me representa', 1, 7);

insert into media(id, nombre, matricula, url, src, id_tmedia, titulo, activo, nro_orden)values
    (8, 'Imagen', 'Y12954', NULL, 'imagen que no subio', 2, 'Imagen que me representa', 1, 8);

insert into media(id, nombre, matricula, url, src, id_tmedia, titulo, activo, nro_orden)values
    (9, 'Imagen', 'Y18433', NULL, '/Images/mis_sueños.jpeg', 2, 'Imagen que me representa', 1, 9);

insert into media(id, nombre, matricula, url, src, id_tmedia, titulo, activo, nro_orden)values
    (10, 'Imagen', 'Y26230', NULL, '/Images/Extrovertida.jpg', 2, 'Imagen que me representa', 1, 10);



insert into media(id, nombre, matricula, url, src, id_tmedia, titulo, activo, nro_orden)values
    (11, 'Dibujo', 'Y23715', NULL, '/Images/bananamichi.png', 3, 'Dibujo', 1, 11);

insert into media(id, nombre, matricula, url, src, id_tmedia, titulo, activo, nro_orden)values
    (12, 'Dibujo', 'UG0095', NULL, '/Images/imagen2Diego.jpeg', 3, 'Dibujo', 1, 12);

insert into media(id, nombre, matricula, url, src, id_tmedia, titulo, activo, nro_orden)values
    (13, 'Dibujo', 'Y12954', NULL, 'imagen que no subio', 3, 'Dibujo', 1, 13);

insert into media(id, nombre, matricula, url, src, id_tmedia, titulo, activo, nro_orden)values
    (14, 'Dibujo', 'Y18433', NULL, '/Images/DibujoNico.png', 3, 'Dibujo', 1, 14);

insert into media(id, nombre, matricula, url, src, id_tmedia, titulo, activo, nro_orden)values
    (15, 'Dibujo', 'Y26230', NULL, '/Images/Dibujo_Zuanny.png', 3, 'Dibujo', 1, 15);


INSERT INTO usuarios (id, email, password, super_usuario, matricula) VALUES
    (1,'prueba@gmail.com', '$2b$10$nlSz1AMKAiF.czm5dNtaJe5j8FAltTFezQfnewJQe2rn8Vh1wdy9m', 1, 'Y23715');











