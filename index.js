const express = require("express");
const bodyParser = require("body-parser");
const hbs = require("hbs");
const session = require("express-session");
const routerAdmin = require("./routes/admin");
const router = require("./routes/public");
const routerAuth = require('./routes/auth');
const app = express();

app.use(session({
    secret: 'your-secret-key',
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false } // asegúrate de cambiar a true en producción con HTTPS
}));

// Middleware para establecer isAuthenticated
app.use((req, res, next) => {
    res.locals.isAuthenticated = !!req.session.userId;
    next();
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname + "/public"));

// Configuración de Handlebars
app.set('view engine', 'hbs');
app.set('views', __dirname + "/views");
hbs.registerPartials(__dirname + "/views/partials");

// Rutas
app.use("/", router);
app.use("/admin", routerAdmin);
app.use("/auth", routerAuth);

// Controlador de inicio
const adminController = {
    index: (req, res) => {
        try {
            // Establecer la sesión del usuario
            req.session.nombre = "Gabriela";
            req.session.logueado = false;
            res.render("admin/index");
        } catch (error) {
            console.error("Error en el controlador de inicio:", error);
            res.status(500).send("Error interno del servidor");
        }
    }
};

// Ruta por defecto para manejar 404
app.use((req, res, next) => {
    res.status(404).render('aviso'); // Renderizar el archivo aviso.hbs desde la carpeta views
});

// Iniciar el servidor
const puerto = process.env.PORT || 3000;
app.listen(puerto, () => {
    console.log("El servidor se está ejecutando en http://localhost:" + puerto);
});

module.exports = adminController;
