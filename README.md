# 03-Y1825-motor-plantillas

## Clonar el repositorio

Para clonar el repositorio a tu máquina local, sigue estos pasos:

1. Abre una terminal.

2. Navega al directorio donde deseas clonar el repositorio.

3. Ejecuta el siguiente comando:

```bash
git clone https://gitlab.com/gabialarcone/03-y-17825-motor-plantillas
```

## Creación de la base de datos

Para crear la base de datos necesaria para este proyecto, sigue estos pasos:

1. Asegúrate de tener instalado SQLite3 en tu sistema. Si no lo tienes, puedes descargarlo desde [aquí](https://www.sqlite.org/download.html). Utiliza el que sea compatible con tu sistema operativo.

2. Abre una terminal y navega hasta el directorio donde se encuentra el archivo `crebas.sql`.

3. Ejecuta el siguiente comando para crear la base de datos:

```bash
sqlite3 integrantes.sqlite < crebas.sql
```
Este comando creará una base de datos SQLite llamada `integrantes.sqlite` utilizando el esquema definido en el archivo `crebas.sql`.

## Configuración del proyecto

Para configurar el proyecto para usar la base de datos que acabas de crear, sigue estos pasos:

1. Asegúrate de tener instalado Node.js y npm en tu sistema. Si no los tienes, puedes descargarlos desde [aquí](https://nodejs.org/en/download/).

2. Abre una terminal y navega hasta el directorio del proyecto.

3. Instala las dependencias del proyecto con el siguiente comando:

```bash
npm install
```
4. Asegúrate de que el archivo `db/conexion.js` está configurado para usar la base de datos `integrantes.sqlite`. Debería tener una línea de código que se vea así:

```javascript
const db = new sqlite3.Database(
    "./db/integrantes.sqlite",
    sqlite3.OPEN_READWRITE,
    (error) => {
        if (error) {
            console.error('Error al conectar con la base de datos:', error.message);
        } else {
            console.log('Conexión exitosa con la base de datos.');
            db.run("select * from integrantes")
        }
    }
);
```
Si todo está configurado correctamente, ahora deberías poder ejecutar el proyecto y ver los datos de la base de datos.

## Ejecución del proyecto

Para ejecutar el proyecto, sigue estos pasos:

1. Abre una terminal y navega hasta el directorio del proyecto.

2. Ejecuta el siguiente comando:

```bash
npm start
```

Ahora deberías poder ver el proyecto ejecutándose en tu navegador en `http://localhost:3000`.