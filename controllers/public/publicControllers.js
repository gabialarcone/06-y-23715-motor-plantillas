
// controllers/public/publicControllers.js
const { getAll } = require("../../db/conexion");

const PublicControllers = {
    renderIndex: async (req, res) => {
        const rows = await getAll("select * from integrante");
        console.log(rows);
        console.log(req.session);
        res.render("index", {
            integrantes: rows,
            isHome: true,
            logueado: req.session.userInfo?.logueado
        });
    },

    renderCurso: async (req, res) => {
        const rows = await getAll("select * from integrante");
        console.log(rows);
        res.render("curso", {
            integrantes: rows,
        });
    },

    renderWordCloud: async (req, res) => {
        const rows = await getAll("select * from integrante");
        console.log(rows);
        res.render("word_cloud", {
            integrantes: rows,
        });
    },

    getIntegranteByMatricula: async (req, res, next) => {
        const matriculas = (await getAll("select matricula from integrante where activo = 1 order by nro_orden")).map(obj => obj.matricula);
        const tipoMedia = await getAll("select * from tipomedia where activo = 1 order by nro_orden");
        const matricula = req.params.matricula;
        const integrantes = await getAll("select * from integrante");

        if (matriculas.includes(matricula)) {
            const integranteFilter = await getAll("select * from integrante where matricula = ?", [matricula]);
            const mediaFilter = await getAll("select * from media where matricula = ?", [matricula]);
            res.render('integrantes', {
                integrante: integranteFilter,
                tipoMedia: tipoMedia,
                media: mediaFilter,
                integrantes: integrantes,
            });
        } else {
            console.log("error");
        }
    },
};

module.exports = PublicControllers;