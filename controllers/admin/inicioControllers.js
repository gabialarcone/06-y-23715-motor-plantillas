const {request} = require("express-session");

const inicioController = {
    index: (req, res) => {
        //crear una session del usuario
        req.session.nombre = "Gabriela";
        req.session.logueado = false;
        req.session.userInfo = {
            nombre: "Gabriela",
            logueado: false,
        };


        res.render("admin/index");
    }
}

module.exports = inicioController;