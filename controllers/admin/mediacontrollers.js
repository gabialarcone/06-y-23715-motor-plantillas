const Joi = require('joi');
const multer = require('multer');
const fs = require("fs");
const path = require('path');
const MediaModel = require('../../models/media.model');
const { mediaSchema } = require('../../validators/media/create');
const { mediaEditSchema } = require('../../validators/media/edit');

// Configurar multer para la carga de archivos
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'public/images/uploads/');
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + path.extname(file.originalname));
    }
});

const upload = multer({ storage: storage });

const MediaController = {
    index: async function (req, res) {
        try {
            const filters = req.query['s'] || {};
            const rows = await MediaModel.getAllMedia(filters);
            res.render('admin/media/index', { media: rows });
        } catch (error) {
            console.error('Error al listar media:', error);
            res.status(500).send('Error al listar media');
        }
    },

        create: async function (req, res) {
            try {
                const integrantes = await MediaModel.getIntegrantes();
                const tiposMedia = await MediaModel.getTiposMedia();
                res.render('admin/media/crearMedia', { integrantes, tiposMedia, errors: [], data: {} });
            } catch (error) {
                console.error('Error al cargar los datos necesarios:', error);
                res.status(500).send('Hubo un error al cargar los datos necesarios. Por favor, inténtelo de nuevo.');
            }
        },

        store: async function (req, res) {
            try {
                if (req.body.activo == "1")
                    req.body.activo = true;
                else if (req.body.activo == "0") req.body.activo = false;

                const { error, value } = mediaSchema.validate(req.body, { abortEarly: false });

                if (error) {
                    const errors = error.details.map(err => err.message);
                    const integrantes = await MediaModel.getIntegrantes();
                    const tiposMedia = await MediaModel.getTiposMedia();
                    return res.status(400).render('admin/media/crearMedia', {
                        errors,
                        data: req.body,
                        integrantes,
                        tiposMedia
                    });
                }

                let newPath = '';
                if (req.file) {
                    newPath = `/images/uploads/${req.file.filename}`;
                }

                if (!newPath && !value.url) {
                    const integrantes = await MediaModel.getIntegrantes();
                    const tiposMedia = await MediaModel.getTiposMedia();
                    return res.status(400).render('admin/media/crearMedia', {
                        errors: ['Debe proporcionar una URL o cargar una imagen.'],
                        data: req.body,
                        integrantes,
                        tiposMedia
                    });
                }

                await MediaModel.createMedia({ ...value, src: newPath });

                res.redirect(`/admin/media/listar?success=${encodeURIComponent('¡Registro insertado correctamente!')}`);
            } catch (error) {
                console.error('Error al crear media:', error);
                res.status(500).json({ error: 'Error al crear media' });
            }
        },

    show: async function (req, res) {
        try {
            const id = req.params.id;
            const media = await MediaModel.getMediaById(id);
            if (media.length === 0) {
                return res.status(404).json({ error: 'Media no encontrado' });
            }
            res.json(media[0]);
        } catch (error) {
            console.error(error);
            res.status(500).json({ error: 'Hubo un error al cargar media. Por favor, inténtelo de nuevo.' });
        }
    },

    destroy: async function (req, res) {
        const id = req.params.id;
        try {
            await MediaModel.deactivateMedia(id);
            res.redirect('/admin/media/listar');
        } catch (error) {
            console.error('Error al actualizar el estado de media:', error);
            res.status(500).json({ message: 'Error al actualizar el estado de media' });
        }
    },

        edit: async function (req, res) {
            try {
                const id = req.params.id;
                const [media] = await MediaModel.getMediaById(id);
                const integrantes = await MediaModel.getIntegrantes();
                const tiposMedia = await MediaModel.getTiposMedia();
                let errorMessage = req.query.error; // Obtener mensajes de error del query string

                if (!media) {
                    return res.status(404).send('Media no encontrado');
                }

                integrantes.forEach(integrante => {
                    integrante.selected = integrante.matricula === media.matricula;
                });

                tiposMedia.forEach(tipo => {
                    tipo.selected = tipo.id_tmedia === media.id_tmedia ? 'selected' : '';
                });

                res.render('admin/media/editMedia', {
                    media,
                    integrantes,
                    tiposMedia,
                    errorMessage // Pasar mensajes de error a la vista
                });
            } catch (error) {
                console.error('Error al obtener el media:', error);
                res.status(500).send('Error al obtener el media');
            }
        },

    update: async function (req, res) {
        try {
            const id = parseInt(req.params.id);

            if (req.body.activo == "1")
                req.body.activo = true;
            else if (req.body.activo == "0") req.body.activo = false;

            const { url, titulo, id_tmedia, matricula, activo } = req.body;
            let newPath = '';

            const { error, value } = mediaEditSchema.validate({ url, titulo, id_tmedia, matricula, activo }, { abortEarly: false });

            if (error) {
                const errores = error.details.map(err => err.message);
                return res.redirect(`/admin/media/edit/${id}?error=${encodeURIComponent(errores.join(';'))}`);
            }

            if (req.file) {
                newPath = `/images/uploads/${req.file.filename}`;
                const tpm_path = req.file.path;
                const destino = "public" + newPath;
                try {
                    await fs.promises.rename(tpm_path, destino);
                } catch (err) {
                    return res.status(500).json({ error: 'Error al mover el archivo' });
                }
            }

            const updateData = { ...value };
            if (newPath) {
                updateData.src = newPath;
            }

            await MediaModel.updateMedia(id, updateData);

            res.redirect(`/admin/media/listar?success=${encodeURIComponent('¡Registro editado correctamente!')}`);
        } catch (error) {
            console.error('Error al editar media:', error);
            res.status(500).json({ error: 'Error al editar media' });
        }
    }
};

module.exports = MediaController;
