const IntegranteModel = require('../../models/integrante.model');
const { integranteSchema } = require('../../validators/integrante/create');
const { integranteEditSchema } = require('../../validators/integrante/edit');

const IntegrantesController = {
    // index - listado
    index: async function (req, res) {
        try {
            let query = 'SELECT * FROM integrante WHERE activo = 1';
            const params = [];

            if (req.query['s']) {
                for (const prop in req.query['s']) {
                    if (req.query['s'][prop]) {
                        if (prop === 'nombre' || prop === 'apellido') {
                            query += ` AND ${prop} LIKE ?`;
                            params.push(`%${req.query['s'][prop]}%`);
                        } else {
                            query += ` AND ${prop} = ?`;
                            params.push(req.query['s'][prop]);
                        }
                    }
                }
            }

            const rows = await IntegranteModel.getAllIntegrantes(query, params);
            res.render('admin/integrante/index', {
                integrantes: rows,
            });
        } catch (error) {
            console.error('Error al listar integrantes:', error);
            res.status(500).send('Error al listar integrantes');
        }
    },

    // create - formulario de creación
    create: function (req, res) {
        res.render("admin/integrante/crearForm", { data: {}, errors: [] });

    },

    // store - método de guardar en la base de datos
    store: async function (req, res) {
        try {
            req.body.activo = req.body.activo === "1" || req.body.activo === "true";

            const { error, value } = integranteSchema.validate(req.body, { abortEarly: false });

            if (error) {
                const errors = error.details.map(err => err.message);
                console.log("errores", error)
                return res.status(400).render('admin/integrante/crearForm', {
                    errors,
                    data: req.body,
                });
            }

            const lastOrder = await IntegranteModel.getLastOrder();
            const newOrder = lastOrder + 1;
            const { nombre, apellido, matricula, pagina, activo } = value;

            await IntegranteModel.insertIntegrante([newOrder, matricula, nombre, apellido, pagina, activo]);

            res.redirect(`/admin/integrante/listar?success=${encodeURIComponent('¡Registro insertado correctamente!')}`);
        } catch (error) {
            console.error(error);
            res.redirect(`/admin/integrante/create?error=${encodeURIComponent('¡Error al insertar el registro!')}`);
        }
    },

    // show - formulario de ver un registro
    show: async function (req, res) {
        try {
            const matricula = req.params.matricula;
            const integrante = await IntegranteModel.getIntegranteByMatricula(matricula);
            if (!integrante) {
                return res.status(404).json({ error: 'Integrante no encontrado' });
            }
            res.json(integrante);
        } catch (error) {
            console.error(error);
            res.status(500).json({ error: 'Hubo un error al cargar el integrante. Por favor, inténtelo de nuevo.' });
        }
    },

    // destroy - operación de eliminar un registro
    destroy: async function (req, res) {
        const matricula = req.params.matricula;

        try {
            await IntegranteModel.deactivateIntegrante(matricula);
            res.redirect('/admin/integrante/listar');
        } catch (error) {
            console.error('Error al actualizar el estado del integrante:', error);
            res.status(500).json({ message: 'Error al actualizar el estado del integrante' });
        }
    },

    // edit - formulario de edición
    edit: async function (req, res) {
        try {
            const matricula = req.params.matricula;
            const integrante = await IntegranteModel.getIntegranteByMatricula(matricula);
            if (!integrante) {
                return res.status(404).send('Integrante no encontrado');
            }
            res.render('admin/integrante/editForm', {
                integrante: integrante,
                errors: []
            });
        } catch (error) {
            console.error('Error al obtener el integrante:', error);
            res.status(500).send('Error al obtener el integrante');
        }
    },

    // update - método para actualizar un registro
    update: async function (req, res) {
        const matricula = req.params.matricula;

        try {
            req.body.activo = req.body.activo === "1" || req.body.activo === "true";

            const { error, value } = integranteEditSchema.validate(req.body, { abortEarly: false });

            if (error) {
                const errors = error.details.map(err => err.message);
                return res.status(400).render('admin/integrante/editForm', {
                    errors,
                    integrante: { ...req.body, matricula }
                });
            }

            await IntegranteModel.updateIntegrante([value.nombre, value.apellido, matricula]);
            res.redirect(`/admin/integrante/listar?success=${encodeURIComponent('¡Registro editado correctamente!')}`);
        } catch (error) {
            console.error('Error al actualizar el integrante:', error);
            res.status(500).json({ message: 'Error al actualizar el integrante' });
        }
    }
};

module.exports = IntegrantesController;
