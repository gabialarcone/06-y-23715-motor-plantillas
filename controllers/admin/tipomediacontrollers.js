const { tipomediaSchema } = require('../../validators/tipomedia/create');
const { tipoMediaEditSchema } = require('../../validators/tipomedia/edit');
const TipoMediaModel = require('../../models/tipomedia.model');

const TipoMediaController = {
    // index - listado
    index: async function (req, res) {
        try {
            const filters = req.query['s'] || {};
            const rows = await TipoMediaModel.getAllTipoMedia(filters);
            res.render("admin/tipomedia/index", {
                tipomedia: rows,
            });
        } catch (error) {
            console.error("Error fetching tipomedia data:", error);
            res.status(500).send("Error fetching data");
        }
    },

    // create - formulario de creación
    create: function (req, res) {
        res.render('admin/tipomedia/crearTipoMedia', { data: {}, errors: [] });
    },

    // store - método de guardar en la base de datos
    store: async function (req, res) {
        try {
            req.body.activo = req.body.activo === "1" || req.body.activo === "true";

            const { error, value } = tipomediaSchema.validate(req.body, { abortEarly: false });

            if (error) {
                const errors = error.details.map(err => err.message);
                return res.status(400).render('admin/tipomedia/crearTipoMedia', {
                    errors,
                    data: req.body,
                });
            }

            await TipoMediaModel.createTipoMedia(value);
            res.redirect(`/admin/tipomedia/listar?success=${encodeURIComponent('¡Registro insertado correctamente!')}`);
        } catch (error) {
            console.error('Error al crear tipo media:', error);
            res.status(500).json({ error: 'Error al crear tipo media' });
        }
    },

    // show - formulario de ver un registro
    show: async function (req, res) {
        try {
            const id_tmedia = req.params.id_tmedia;
            const tipomedia = await TipoMediaModel.getTipoMediaById(id_tmedia);
            if (tipomedia.length === 0) {
                return res.status(404).json({ error: 'Tipo de media no encontrado' });
            }
            res.json(tipomedia[0]);
        } catch (error) {
            console.error(error);
            res.status(500).json({ error: 'Hubo un error al cargar el tipo de media. Por favor, inténtelo de nuevo.' });
        }
    },

    // destroy - operación de eliminar un registro
    destroy: async function (req, res) {
        const id_tmedia = req.params.id_tmedia;
        try {
            await TipoMediaModel.deactivateTipoMedia(id_tmedia);
            res.redirect('/admin/tipomedia/listar');
        } catch (error) {
            console.error('Error al actualizar el estado del media:', error);
            res.status(500).json({ message: 'Error al actualizar el estado del media' });
        }
    },

    // edit - formulario de edición
    edit: async function (req, res) {
        try {
            const id_tmedia = req.params.id_tmedia;
            const tipomedia = await TipoMediaModel.getTipoMediaById(id_tmedia);
            if (!tipomedia) {
                return res.status(404).send('Tipo media no encontrado');
            }
            res.render('admin/tipomedia/editTipoMedia', {
                tipomedia,
                errors: [],
            });
        } catch (error) {
            console.error('Error al obtener el tipo media:', error);
            res.status(500).send('Error al obtener el tipo media');
        }
    },

    // update - método de editar un registro
    update: async function (req, res) {
        const id_tmedia = req.params.id_tmedia;
        req.body.activo = req.body.activo === "1" || req.body.activo === "true";

        const { error, value } = tipoMediaEditSchema.validate(req.body, { abortEarly: false });

        if (error) {
            const errors = error.details.map(err => err.message);
            return res.status(400).render('admin/tipomedia/editTipoMedia', {
                errors,
                tipomedia: { ...req.body, id_tmedia },
            });
        }

        try {
            await TipoMediaModel.updateTipoMedia(id_tmedia, value);
            res.redirect(`/admin/tipomedia/listar?success=${encodeURIComponent('¡Registro actualizado correctamente!')}`);
        } catch (error) {
            console.error('Error al actualizar el tipo media:', error);
            res.status(500).json({ error: '¡Error al actualizar el tipo media!' });
        }
    }
};

module.exports = TipoMediaController;
